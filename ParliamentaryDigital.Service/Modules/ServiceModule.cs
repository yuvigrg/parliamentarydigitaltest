﻿using Ninject.Modules;
using ParliamentaryDigital.Service.Interface;

namespace ParliamentaryDigital.Service.Modules
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IBusinessService>().To<BusinessService>().InThreadScope();
            Bind<WebService>().ToSelf();
        }
    }
}
