﻿using System.Net;

namespace ParliamentaryDigital.Service
{
    public class WebService
    {
        public string GetData(string url)
        {
            using (var wc = new WebClient())
            {
                return wc.DownloadString(url);
            }
        }
    }
}
