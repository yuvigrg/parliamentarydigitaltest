﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace ParliamentaryDigital.Service.Models
{
    [XmlRoot("ArrayOfEvent")]
    public class Events
    {
        public Events()
        {
            Businesses = new List<Business>();
        }
        [XmlElement("Event")]
        public List<Business> Businesses { get; set; }

        [Display(Name = "Choose your Start Date")]
        public string StartDate { get; set; }
        [Display(Name = "End Date")]
        public string EndDate { get; set; }
    }

    [XmlRoot("Event")]
    public class Business
    {
        public Business()
        {
            Members = new Members();
        }
        [XmlAttribute("Id")]
        public string Id { get; set; }
        [XmlElement("Description")]
        public string Description { get; set; }
        [XmlElement("StartDate")]
        public string StartDate { get; set; }
        [XmlElement("StartTime")]
        public string StartTime { get; set; }
        [XmlElement("EndDate")]
        public string EndDate { get; set; }
        [XmlElement("EndTime")]
        public string EndTime { get; set; }
        [XmlElement("Members")]
        public Members Members { get; set; }
        [XmlElement("Category")]
        public string Category { get; set; }
        [XmlElement("Type")]
        public string Type { get; set; }
        [XmlElement("House")]
        public string House { get; set; }
    }
}
