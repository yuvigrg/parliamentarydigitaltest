﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParliamentaryDigital.Service.Models
{
    [XmlRoot("Members")]
    public class Members
    {
        [XmlElement("Member")]
        public List<Member> MemberList { get; set; }
    }

    [XmlRoot("Member")]
    public class Member
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }

        [XmlElement("Party")]
        public string Party { get; set; }

        [XmlElement("MemberFrom")]
        public string MemberFrom { get; set; }

        [XmlElement("FullTitle")]
        public string FullTitle { get; set; }
    }
}
