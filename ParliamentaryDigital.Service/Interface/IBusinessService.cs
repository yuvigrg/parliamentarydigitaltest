﻿using System.Collections.Generic;
using ParliamentaryDigital.Service.Models;

namespace ParliamentaryDigital.Service.Interface
{
    public interface IBusinessService
    {
        Events GetBusinesses();
        Events GetBusinesses(string startDate, string endDate);
        Business GetBusiness(string id, string startDate, string endDate);
    }
}
