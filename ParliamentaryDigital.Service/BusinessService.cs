﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using log4net;
using ParliamentaryDigital.Service.Interface;
using ParliamentaryDigital.Service.Models;
using static System.DayOfWeek;

namespace ParliamentaryDigital.Service
{
    public class BusinessService : IBusinessService
    {
        private readonly WebService _webService;
        private readonly ILog _log;

        public BusinessService(WebService webService)
        {
            _webService = webService;
            _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        public Events GetBusinesses()
        {
            //default call
            var today = DateTime.Today;
            var endDay = DateTime.Today.DayOfWeek;
            string startDate;
            switch (endDay)
            {
                case Sunday:
                    startDate = today.AddDays(-6).ToString("yyyy-MM-dd");
                    break;
                case Saturday:
                    startDate = today.AddDays(-5).ToString("yyyy-MM-dd");
                    break;
                case Friday:
                    startDate = today.AddDays(-4).ToString("yyyy-MM-dd");
                    break;
                case Thursday:
                    startDate = today.AddDays(-3).ToString("yyyy-MM-dd");
                    break;
                case Wednesday:
                    startDate = today.AddDays(-2).ToString("yyyy-MM-dd");
                    break;
                case Tuesday:
                    startDate = today.AddDays(-1).ToString("yyyy-MM-dd");
                    break;
                case Monday:
                    startDate = today.ToString("d");
                    break;
                default:
                    return null;

            }
            var endDate = today.ToString("yyyy-MM-dd");


            //default week
            //tried the URL as documented In http://service.calendar.parliament.uk/Help/Event. Was not sure any of those were working
            //var url = "http://service.calendar.parliament.uk/calendar/events/list.xml?date=thisweek";
            //hence created my own

            var url = $"http://service.calendar.parliament.uk/calendar/events/list.xml?startdate={startDate}&enddate={endDate}";

            var val = _webService.GetData(url);

            if (!string.IsNullOrEmpty(val))
            {
                try
                {
                    var xmlDoc = XDocument.Parse(val);

                    var events = Deserialize<Events>(xmlDoc);

                    return new Events
                    {
                        Businesses = events.Businesses.Where(x => x.Type == "Main Chamber" && x.House == "Commons").ToList(),
                        EndDate = endDate,
                        StartDate = startDate
                    };
                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message, ex);
                    return new Events();
                }

            }
            return new Events();
        }

        public Events GetBusinesses(string startDate, string endDate)
        {
            //generate url 
            var url = $"http://service.calendar.parliament.uk/calendar/events/list.xml?startdate={startDate}&enddate={endDate}";

            var val = _webService.GetData(url);

            if (!string.IsNullOrEmpty(val))
            {
                try
                {
                    var xmlDoc = XDocument.Parse(val);

                    var events = Deserialize<Events>(xmlDoc);

                    return new Events
                    {
                        Businesses = events.Businesses.Where(x => x.Type == "Main Chamber" && x.House == "Commons").ToList(),
                        EndDate = endDate,
                        StartDate = startDate
                    };
                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message,ex);
                    return new Events();
                }

            }
            return new Events();
        }

        public Business GetBusiness(string id,string startDate, string endDate)
        {
            var url = $"http://service.calendar.parliament.uk/calendar/events/list.xml?startdate={startDate}&enddate={endDate}";          

            var val = _webService.GetData(url);

            if (string.IsNullOrEmpty(val)) return new Business();
            try
            {               
                var xmlDoc = XDocument.Parse(val);
                var events = Deserialize<Events>(xmlDoc);
                var i = events.Businesses.Find(x => x.Id == id);

                if (i.Members.MemberList.Any())
                {
                    var members = new Members
                    {
                        MemberList = new List<Member>()
                    };
                    foreach (var mem in i.Members.MemberList)
                    {
                        var member = GetMember(mem.Id);
                        members.MemberList.Add(member);
                    }

                    return new Business
                    {
                        Members = members,
                        Category = i.Category
                    };
                }
                return new Business
                {
                    Members = new Members
                    {
                        MemberList = new List<Member>
                        {
                            new Member
                            {
                                
                            }
                        }
                    },
                    Category = i.Category
                };
                    
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return new Business();
            }
        }

        private Member GetMember(string id)
        {
            if(string.IsNullOrEmpty(id)) return new Member();
            var memUrl = $"http://data.parliament.uk/membersdataplatform/services/mnis/members/query/id={id}";

            var val = _webService.GetData(memUrl);

            var xmlDoc = XDocument.Parse(val);

            var members = Deserialize<Members>(xmlDoc);
            return members.MemberList[0];
        }


        private static T Deserialize<T>(XDocument xmlDocument)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var reader = xmlDocument.CreateReader())
            {
                return (T)xmlSerializer.Deserialize(reader);
            }
        }
    }
}
