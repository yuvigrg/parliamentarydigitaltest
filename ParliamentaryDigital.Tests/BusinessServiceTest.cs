﻿using Moq;
using ParliamentaryDigital.Service;
using Xunit;

namespace ParliamentaryDigital.Tests
{
    public class BusinessServiceTest
    {       
        [Fact]
        public void GetEvents()
        {
            //arrange
            var webServiceMock = new Mock<WebService>();
            var startDate = "2015-11-16";
            var endDate = "2015-11-20";

            var service = new BusinessService(webServiceMock.Object);
            
            //act
            var response = service.GetBusinesses(startDate, endDate);

            //assert
            Assert.NotNull(response);
            Assert.Equal(response.StartDate,startDate);
            Assert.Equal(response.EndDate,endDate);
            Assert.Equal(response.Businesses.Count,27);
        }

        [Fact]
        public void GetEventDetails()
        {
            //arrange
            var webServiceMock = new Mock<WebService>();

            var fullTitle = "The Rt Hon. the Lord Foulkes of Cumnock";
            var party = "Labour";
            var memberFrom = "Life peer";
            var category = "Legislation";
            var eventId = "6053";
            var startDate = "2015-11-16";
            var endDate = "2015-11-20";

            var service = new BusinessService(webServiceMock.Object);

            //act
            var response = service.GetBusiness(eventId, startDate, endDate);

            //assert

            Assert.NotNull(response);
            Assert.Equal(response.Category,category);
            Assert.Equal(response.Members.MemberList[0].FullTitle,fullTitle);
            Assert.Equal(response.Members.MemberList[0].Party, party);
            Assert.Equal(response.Members.MemberList[0].MemberFrom, memberFrom);
        }
    }
}
