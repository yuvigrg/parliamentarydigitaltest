﻿using System.Web.Mvc;
using ParliamentaryDigital.Service.Interface;
using ParliamentaryDigital.Service.Models;

namespace ParliamentaryDigital.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBusinessService _service;

        public HomeController(IBusinessService service)
        {
            _service = service;
        }

        public ActionResult Index(string startDate, string endDate)
        {
            Events model;

            if (!string.IsNullOrEmpty(startDate) && !(string.IsNullOrEmpty(endDate)))
            {
                model = _service.GetBusinesses(startDate, endDate);
            }
            else
            {
                model = _service.GetBusinesses();
            }            
            return View(model);
        }
        
        [HttpPost]
        public JsonResult EventDetails(string id, string startDate, string endDate)
        {
            var model = _service.GetBusiness(id, startDate, endDate);          
            return Json(model);
        }

    }
}